export default {
    install (Vue, options) {
        try {
            Vue.prototype.$db = openDatabase(options.name, '1.0', options.displayName, options.size * 1024 * 1024)
        } catch (e) {
            Vue.prototype.$db = false
        }
    }
}
