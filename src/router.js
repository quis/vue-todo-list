import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [
        {
            path: "/",
            name: "list",
            component: () =>
                import("./views/TodoList.vue")
        },
        {
            path: "/add",
            name: "add",
            component: () =>
                import("./views/TodoAdd.vue")
        },
        {
            path: "/view/:id",
            name: "view",
            component: () =>
                import("./views/TodoView.vue")
        }
    ]
});
