import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        status: 'none',
        todos: [],
        count: 0,
        dbAvailable: false
    },
    mutations: {
        setTodos(state, todos) {
            for (var idx = 0; idx < state.todos.length; idx++) {
                Vue.delete(state.todos, idx)
            }
            state.count = 0
            for (var idx = 0; idx < todos.length; idx++) {
                state.todos.push(todos[idx])
                state.count++
            }
        },
        updateElement(state, data) {
            for (var field in data) {
                if (field === 'id') continue
                state.todos[data.id][field] = data[field]
            }
        },
        updateStatus(state, status) {
            state.status = status
        },
        setDbAvailablity(state, status) {
            state.dbAvailable = status
        },
        setStorageAvailablity(state, status) {
            state.storageAvailable = status
        }
    },
    getters: {
        getElement: (state) => (id) => {
            if (typeof id !== 'number') {
                try {
                    id = parseInt(id)
                } catch (e) {
                    console.error('Invalid element id given: ' + id)
                }
            }
            return state.todos.find((todo) => {
                return todo.id === id
            })
        },
        getStatus: (state) => {
            return state.status
        },
        getCount: (state) => {
            return state.count
        }
    },
    actions: {
        noDbAvailable() {
            return new Promise((resolve, reject) => {
                console.error('Web SQL is not available')
                reject()
            })
        },
        updateLocalStorage(store) {
            if (store.state.storageAvailable) {
                localStorage.setItem('lastUpdate', Date.now())
            }
        },
        loadTodos(store, db) {
            if (db) {
                store.commit('updateStatus', 'pending')
                return new Promise((resolve, reject) => {
                    db.transaction(function (tx) {
                        tx.executeSql("CREATE TABLE IF NOT EXISTS todos (id INTEGER PRIMARY KEY NOT NULL, date_created DATETIME NOT NULL, date_modified DATETIME NOT NULL, name VARCHAR(128) NOT NULL, description TEXT, done BOOLEAN DEFAULT(0));");
                        tx.executeSql("SELECT * FROM todos ORDER BY done ASC, date_modified DESC", [], function (tx, result) {
                            store.commit("setTodos", result.rows)
                            store.commit('updateStatus', 'loaded')
                            resolve()
                        });
                    })
                })
            } else {
                return store.dispatch('noDbAvailable')
            }
        },
        addElement(store, params) {
            if (params.db) {
                return new Promise((resolve, reject) => {
                    params.db.transaction(function (tx) {
                        tx.executeSql("INSERT INTO todos (date_created, date_modified, name, description) VALUES(?,?,?,?)", [params.data.date_created, params.data.date_modified, params.data.name, params.data.description], function (tx, result) {
                            //store.dispatch('loadTodos', params.db)
                            store.dispatch('updateLocalStorage')
                            resolve(result.insertId)
                        })
                    })
                })
            } else {
                return store.dispatch('noDbAvailable')
            }
        },
        updateElement(store, params) {
            if (params.db) {
                return new Promise((resolve, reject) => {
                    params.db.transaction(function (tx) {
                        var fields = []
                        for (var field in params.data) {
                            if (params.data[field] === true) {
                                params.data[field] = 1
                            } else if (params.data[field] === false) {
                                params.data[field] = 0
                            }
                            fields.push(field + "='" + params.data[field] + "'")
                        }
                        tx.executeSql("UPDATE todos SET " + fields.join(',') + " WHERE id=?;", [params.id], function (tx, result) {
                            //store.dispatch('loadTodos', params.db)
                            store.dispatch('updateLocalStorage')
                            resolve()
                        })
                    })
                })
            } else {
                return store.dispatch('noDbAvailable')
            }
        },
        deleteElement(store, params) {
            if (params.db) {
                return new Promise((resolve, reject) => {
                    params.db.transaction(function (tx) {
                        tx.executeSql("DELETE FROM todos WHERE id=?", [params.id], function (tx, result) {
                            //store.dispatch('loadTodos', params.db)
                            store.dispatch('updateLocalStorage')
                            resolve()
                        })
                    })
                })
            } else {
                return store.dispatch('noDbAvailable')
            }
        }
    }
});
