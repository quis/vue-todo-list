import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "bootstrap";
import db from "./database"
import moment from "./lib/moment"
import jquery from "./lib/jquery"

Vue.config.productionTip = false;

Vue.use(db, {
    name: 'todolist',
    displayName: 'Database for storing todos',
    size: 3
});

Vue.use(moment)
Vue.use(jquery)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
