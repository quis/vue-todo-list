import jquery from "jquery"

export default {
    install(Vue) {
        Vue.prototype.$$ = jquery
    }
}
